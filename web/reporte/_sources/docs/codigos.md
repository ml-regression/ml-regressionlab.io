---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

(codigos)=

# Códigos


## Firmware Arduino v1.7.0

```{eval-rst}
.. literalinclude:: /_static/src/EQ_V_022.ino
   :language: c
   :caption: Código Arduino v1.7.0
   :name: arduino200
   :emphasize-lines: 90
   :linenos:
```


## Software Python Raspberry Pi

```{eval-rst}
.. literalinclude:: /_static/src/tiny_captorv2.py
   :language: python
   :caption: Código Raspberry Pi comunicación via I2C con Arduino
   :name: tinypythoni2c
   :emphasize-lines: 23,43,56,95,112,150
   :linenos:
```