---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.12
    jupytext_version: 1.8.2
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

+++ {"tags": []}

# Calibración de sensores

## Introducción

En este trabajo se realizó la calibración de sensores de humedad de suelo de bajo costo en diferentes tipos de suelo. Se utilizó Regresión Lineal Múltimple (MLR) y técnincas de regresión no lineales: K-Nearest Neighbor (KNN), Support Vector regression (SVR) y Random FOrest (RFO). 

Los suelos fueron caracterizados mediante un análisis granulométrico. Para las mediciones en campo se registró humedad y temperatura ambiente, capacitancia con los sensores de bajo costo y temperatura del suelo. El valor de referencia para los ensayos en laboratorio se realizaron mediante gravimetría y para las pruebas de campo se utilizaron sensores digitales Hydraprobe 2 del fabricante Stevens. Para evaluar los resultados obtenidos se utilizó el RMSE (Root Mean Square Error) y el coeficiente de determinación ($R^2$).   

+++ {"tags": ["remove-cell"]}

La humedad de suelo tiene una gran influencia en el desarrollo de los cultivos, un mejor entendimiento de su dinámica contribuirá a mejorar las estimaciones de la oferta hídrica para los cultivos. Múltiples técnicas y dispositivos se han utilizado para determinar el contenido de agua del suelo, sin embargo, las alternativas comerciales siguien siendo de elevado costo lo que dificulta su uso extendido {cite}`Gonzalez-Teruel2019`. El uso de sensores de bajo costo permitiría mejorar la resolución espacial y temporal.

+++

### Análisis del estado del arte

Inicialmente se estudiaron las publicaciones del grupo en la temática {cite}`Cid2019`, {cite}`Ferrer-Cid2019` y {cite}`Ferrer-Cid2020`.
Posteriormente se analizó el estado del arte sobre calibración de sensores de humedad de suelo tanto con técnicas de machine learning como enfoques convencionales. A continuación se listan los trabajos encontrados con una breve despcrición de su contenido.

#### Mediante Machine Learning

- Carranza, C., Nolet, C., Pezij, M., & van der Ploeg, M. (2021). **Root zone soil moisture estimation with Random Forest**. Journal of Hydrology, 593(December 2020), 125840. https://doi.org/10.1016/j.jhydrol.2020.125840

    In this study, the Random Forest (RF) ensemble learning algorithm, is tested to demonstrate the capabilities and advantages of ML for Root zone soil moisture (RZSM) estimation. Interpolation and extrapolation of RZSM on a daily timescale was carried out using RF. RF predictions have slightly higher accuracy for interpolation and similar accuracy for extrapolation in comparison with RZSM simulated from a process-based model.

- Coopersmith, E. J., Cosh, M. H., Bell, J. E., & Boyles, R. (2016). **Using machine learning to produce near surface soil moisture estimates from deeper in situ records at U.S. Climate Reference Network (USCRN) locations: Analysis and applications to AMSR-E satellite validation**. Advances in Water Resources, 98, 122–131. https://doi.org/10.1016/j.advwatres.2016.10.007

    Soil moisture sensors from the U.S. Climate Reference Net- work (USCRN) were used to generate models of 5 cm soil moisture, with 10 cm soil moisture measure- ments and antecedent precipitation as inputs, via machine learning techniques. Validation was conducted with the available, in situ , 5 cm resources.

- Yamaç, S. S., Şeker, C., & Negiş, H. (2020). **Evaluation of machine learning methods to predict soil moisture constants with different combinations of soil input data for calcareous soils in a semi arid area**. Agricultural Water Management, 234(December 2019). https://doi.org/10.1016/j.agwat.2020.106121


    This study evaluated the performance of deep learning (DL), artificial neural network (ANN) and k-nearest neighbour (kNN) models to estimate field capacity (FC) and permanent wilting point (PWP) using four com-binations of soil data.Therefore, the DL model could be recommended for the estimation of FC when full soil data are available and the kNN model could be recommended for estimation of PWP with all combinations of soil data

- Liu, Y., Jing, W., Wang, Q., & Xia, X. (2020). **Generating high-resolution daily soil moisture by using spatial downscaling techniques: a comparison of six machine learning algorithms**. Advances in Water Resources, 141, 103601. https://doi.org/10.1016/j.advwatres.2020.103601

    In this study, the performance of multiple machine learning algorithms in downscaling the Essential Climate Variable Program initiated by the European Space Agency Soil Moisture dataset was validated over different underlying surfaces.Six machine learning algorithms: artificial neural network (ANN), Bayesian (BAYE), classification and regression trees (CART), K nearest neighbor (KNN), random forest (RF), and support vector machine (SVM), were implemented to establish the spatial downscaling models over four case study areas with reliable continuous in-situ SM observations over four case study areas.

- Chen, L., Zhangzhong, L., Zheng, W., Yu, J., Wang, Z., Wang, L., & Huang, C. (2019). **Data-driven calibration of soil moisture sensor considering impacts of temperature: A case study on FDR sensors**. Sensors (Switzerland), 19(20). https://doi.org/10.3390/s19204381

    In this study, the temperature impact on the soil moisture sensor reading was firstly analyzed. Next, a pioneer study on the data-driven calibration of soil moisture sensor was investigated considering the impacts of temperature.
    To our best knowledge, it is the first time that data-driven methods have been applied into the calibration of soil moisture sensor. Two data-driven models including the multivariate adaptive regression splines (MARS) [15,16] and the Gaussian process regression (GPR) [17,18] were developed to calibrate soil moisture sensor considering the impacts of temperature on the accuracy of measurements.

- Achieng, K. O. (2019). **Modelling of soil moisture retention curve using machine learning techniques: Artificial and deep neural networks vs support vector regression models**. Computers and Geosciences, 133(August), 104320. https://doi.org/10.1016/j.cageo.2019.104320

    In this study, plausibility of various machine learning techniques to simulate soil moisture retention curve of loamy sand are evaluated. Specifically, the machine learning techniques that are investigated include: three support vector regression (SVR) models (i.e. radial basis function (RBF), linear and polynomial kernels), single-layer artificial neural network (ANN), and deep neural network (DNN). The soil moisture and soil suction were measured using time-domain reflectometer (TDR) and tensiometer, respectively.

- Chen, S., She, D., Zhang, L., Guo, M., & Liu, X. (2019). **Spatial downscaling methods of soil moisture based on multisource remote sensing data and its application**. Water (Switzerland), 11(7), 1–25. https://doi.org/10.3390/w11071401

    This study attempted to utilize machine learning and data mining algorithms to downscale the Advanced Microwave Scanning Radiometer-Earth Observing System (AMSR-E) soil moisture data from 25 km to 1 km and compared the advantages and disadvantages of the random forest model and the Cubist algorithm to determine the more suitable soil moisture downscaling method for the middle and lower reaches of the Yangtze River Basin

- Yu, Z., Bedig, A., Montalto, F., & Quigley, M. (2018). **Automated detection of unusual soil moisture probe response patterns with association rule learning**. Environmental Modelling and Software, 105, 257–269. https://doi.org/10.1016/j.envsoft.2018.04.001

    In this paper, we develop a rule-based learning algorithm involving Dynamic Time Warping (DTW) to investigate the feasibility of detecting anomalous responses from soil moisture probes


#### Enfoque convencional
- Domínguez-Niño, J. M., Bogena, H. R., Huisman, J. A., Schilling, B., & Casadesús, J. (2019). On the accuracy of factory-calibrated low-cost soil water content sensors. Sensors (Switzerland), 19(14). https://doi.org/10.3390/s19143101
    
    The aim of this paper is to test the degree of improvement of various sensor- and soil-specific calibration options compared to factory calibrations. The second step involved the establishment of a site-specific relationship between permittivity and soil water content using undisturbed soil samples and time domain reflectometry (TDR) measurements

- Zemni, N., Bouksila, F., Persson, M., Slama, F., Berndtsson, R., & Bouhlila, R. (2019). Laboratory calibration and field validation of soil water content and salinity measurements using the 5TE sensor. Sensors (Switzerland), 19(23), 1–18. https://doi.org/10.3390/s19235272

    the objective of the present study was to assess the performance of the 5TE sensor to estimate soil water content and soil pore electrical conductivity for a representative sandy soil used for cultivation of date palms. Both standard models and a novel approach using corrected models to compensate for high electrical conductivity were used

- Ferrarezi, R. S., Nogueira, T. A. R., & Zepeda, S. G. C. (2020). Performance of soil moisture sensors in Florida Sandy Soils. Water (Switzerland), 12(2), 1–20. https://doi.org/10.3390/w12020358

    A laboratory study was conducted to evaluate the performance of several commercial sensors and to establish soil-specific calibration equations for different soil types.Soil-specific calibrations from this study resulted in accuracy expressed as root mean square error (RMSE)

- Zhou, W., Xu, Z., Ross, D., Dignan, J., Fan, Y., Huang, Y., Wang, G., Bagtzoglou, A. C., Lei, Y., & Li, B. (2019). **Towards water-saving irrigation methodology: Field test of soil moisture profiling using flat thin mm-sized soil moisture sensors (MSMSs)**. Sensors and Actuators, B: Chemical, 298(July), 126857. https://doi.org/10.1016/j.snb.2019.126857

    The 10-month field tests conducted at a farm site compared three groups of MSMS with commercial capacitance-type soil moisture sensors (SMS) in terms of accuracy, sensitivity to environmental variations (e.g. water shock, temperatures, dry/ wet seasons) and long-term stability

- Nagahage, E. A. A. D., Nagahage, I. S. P., & Fujino, T. (2019). **Calibration and validation of a low-cost capacitive moisture sensor to integrate the automated soil moisture monitoring system**. Agriculture (Switzerland), 9(7). https://doi.org/10.3390/agriculture9070141

    We have developed a prototype for automated soil moisture monitoring using a low-cost capacitive soil moisture sensor (SKU:SEN0193) for data acquisition, connected to the internet. A soil-specific calibration was performed to integrate the sensor with the automated soil moisture monitoring system. The accuracy of the soil moisture measurements was compared with those of a gravimetric method and a well-established soil moisture sensor (SM-200, Delta-T Devices Ltd, Cambridge, UK).

- Jiménez, A. de los Á. C., Almeida, C. D. G. C. de, Santos Júnior, J. A,Morais, J. E. F. de, Almeida, B. G. de, & Andrade, F. H. N. de. (2019). **Accuracy of capacitive sensors for estimating soil moisture in northeastern Brazil**. Soil and Tillage Research, 195(September), 104413. https://doi.org/10.1016/j.still.2019.104413

    The objective of this study was to calibrate the YL-69 sensor and compare its accuracy with two commercial ECH2O probes.

- Bogena, H. R., Huisman, J. A., Schilling, B., Weuthen, A., & Vereecken, H. (2017). **Effective calibration of low-cost soil water content sensors**. Sensors (Switzerland), 17(1). https://doi.org/10.3390/s17010208

    Here, we present an effective calibration method to improve the measurement accuracy of low-cost soil water content sensors taking the recently developed SMT100 sensor (Truebner GmbH, Neustadt, Germany) as an example. We calibrated the sensor output of more than 700 SMT100 sensors to permittivity using a standard procedure based on five reference media with a known apparent dielectric permittivity.

- Singh, J., Lo, T., Rudnick, D. R., Dorr, T. J., Burr, C. A., Werle, R., Shaver, T. M., & Muñoz-Arriola, F. (2018). **Performance assessment of factory and field calibrations for electromagnetic sensors in a loam soil**. Agricultural Water Management, 196, 87–98. https://doi.org/10.1016/j.agwat.2017.10.020

    The performance of eight electromagnetic (EM) sensors (TDR315, CS655, HydraProbe2, 5TE, EC5, CS616, Field Connect, AquaCheck), were analyzed through a field study in a loam soil. T, ECa, and ERa were compared in reference to overall average among all sensors, and ?v in reference to a neutron moisture meter (NMM).

- Lo, T. H., Rudnick, D. R., Singh, J., Nakabuye, H. N., Katimbo, A., Heeren, D. M., & Ge, Y. (2020). **Field assessment of interreplicate variability from eight electromagnetic soil moisture sensors**. Agricultural Water Management, 231(January), 105984. https://doi.org/10.1016/j.agwat.2019.105984

- Koley, S., & Jeganathan, C. (2020). **Estimation and evaluation of high spatial resolution surface soil moisture using multi-sensor multi-resolution approach**. Geoderma, 378(February), 114618. https://doi.org/10.1016/j.geoderma.2020.114618

- Serrano, D., Ávila, E., Barrios, M., Darghan, A., & Lobo, D. (2020). **Surface soil moisture monitoring with near-ground sensors: Performance assessment of a matric potential-based method**. Measurement: Journal of the International Measurement Confederation, 155, 107542. https://doi.org/10.1016/j.measurement.2020.107542

- Tan, W. Y., Then, Y. L., Lew, Y. L., & Tay, F. S. (2019). **Newly calibrated analytical models for soil moisture content and pH value by low-cost YL-69 hygrometer sensor**. Measurement: Journal of the International Measurement Confederation, 134, 166–178. https://doi.org/10.1016/j.measurement.2018.10.071

- Kizito, F., Campbell, C. S., Campbell, G. S., Cobos, D. R., Teare, B. L., Carter, B., & Hopmans, J. W. (2008). **Frequency, electrical conductivity and temperature analysis of a low-cost capacitance soil moisture sensor**. Journal of Hydrology, 352(3–4), 367–378. https://doi.org/10.1016/j.jhydrol.2008.01.021

- Santos, W. J. R., Silva, B. M., Oliveira, G. C., Volpato, M. M. L., Lima, J. M., Curi, N., & Marques, J. J. (2014). **Soil moisture in the root zone and its relation to plant vigor assessed by remote sensing at management scale**. Geoderma, 221–222(April), 91–95. https://doi.org/10.1016/j.geoderma.2014.01.006

+++

## Materiales y métodos

### Sensores
Se utilizaron sensores de bajo costo, en adelante denominados capacitivos, cuyo principio de funcionamiento consiste en medir de forma indirecta la constante dieléctrica del suelo, brindando un voltaje analógico inversamente proporcional a la humedad del suelo. 

Estos sensores fueron contrastados de dos maneras, en una primera instancia a través de un ensayo gravimétrico en laboratorio, posteriormente en condiciones ambientales donde se registró en forma simultánea con los sensores Hydraprobe II de Stevens, cuyo excelente desempeño los ha convertido en un estandar de facto para investigaciones científicas y la industria del agro [^hydraprobe]. Ver {numref}`sensoreshpycapa`.

En todos los casos la humedad del suelo fue medida entre los 7 y 10cm de profundidad aproximadamente.

Los sensores Hydraprobe II miden el contenido volumétrico del agua (CVA), conductividad eléctrica y temperatura. Utilizan el protocolo digital SDI-12, el firmware [^firmwarehydraprobe] necesario para comunicarse fue desarrollado para una placa Arduino. 

El CVA es el volumen de agua que puede contener un volumen de suelo. El agua se alojará en los espacios entre las partículas, por lo que su valor máximo será la porosidad del suelo. La humedad del suelo es la proporción ocupada de agua de su porosidad. Suelos arenosos tienen una porosisdad de 0.25 a 0.47 mientras que suelos limosos/arcillosos pueden ir de 0.34 a 0.50.


Para temperatura y humedad ambiente se utilizó el sensor DHT22 [^dht22]. 

```{figure} /_static/img/hp_capacitivos_sensores.png
---
name: sensoreshpycapa
alt: fishy
width: 1000px
align: center
---
Sensores de bajo costo capacitivos (izquierda) e Hydraprobe II (derecha)
```


[^hydraprobe]: [Hydraprobe II Soil Moisture sensor](https://stevenswater.com/products/hydraprobe/)
[^firmwarehydraprobe]:[Arduino code for Hydraprobe](https://gitlab.com/emilopez/pysuelo/-/tree/master/src/arduino/hydraprobe2_base)
[^dht22]: [Sensor DHT22](https://learn.adafruit.com/dht)

### Tipos de suelo
Mediante un análisis granulométrico se determinó la proporción del tamaño de partículas para cada tipo de suelo utilizado en los ensayos ({numref}`Tabla {number}<suelos>`).

**detallar clasificación del suelo: arenoso, limoso, etc **

+++



```{list-table} Proporción del tamaño de partículas para cada tipo de suelo 
:align: center
:header-rows: 1
:name: suelos

* - Tamaño ($\mu m$)
  - Tipo 1 (%)
  - Tipo 2 (%)
  - Tipo 3 (%)
  - Tipo 4 (%)
* - $>$ 1000
  - 0.0126
  - 0.0011
  - 0.0026
  - 0.1032
* - $>$ 250
  - 0.1138
  - 0.0048
  - 0.4859
  - 0.2620
* - $>$ 63
  - 0.5281
  - 0.0306
  - 0.4398
  - 0.3157
* - $<$ 63
  - 0.3455
  - 0.9634
  - 0.0717
  - 0.3191
```


+++ {"tags": []}

En el siguiente mapa se observan los sitios donde se realizaron las pruebas de campo: [mapa muestras de suelo](http://u.osmfr.org/m/586030/)

```{figure} /_static/img/ubicacion_suelos.png
---
name: ubicacionsuelo
alt: muestras de suelo
width: 1000px
align: center
---
Sitios donde se obtuvieron las muestras de suelo
```

```{code-cell} ipython3
:tags: [remove-cell]

from IPython.display import HTML

# Youtube
HTML('<iframe width="100%" height="500px" frameborder="0" allowfullscreen src="http://umap.openstreetmap.fr/es/map/muestras-de-suelo_586030?scaleControl=true&miniMap=true&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=false&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe>')
```

```{code-cell} ipython3

```

### Mediciones

La gravimetría permitió evaluar de modo exploratorio la dinámica de los sensores capacitivos en condiciones controladas, sirvió como punto de partida para evaluarlos posteriormente en condiciones reales. 

#### Gravimetría

Se secaron dos muestras de suelo arenoso (tipo 3, ver {numref}`Tabla {number}<suelos>`) en una estufa a 105ºC por 48 horas posteriormente fueron instaladas sobre dos balanzas de precisión donde se incorporó una cantidad de agua equivalente a su porosidad para saturar completamente las muestras. Durante el proceso de secado se registró automáticamente con una cámara fotográfica los valores de las balanzas y en simultáneo se tomaron las lecturas de los sensores de bajo costo. 

Para vincular las medidas de las balanzas con las lecturas de los sensores se desarrolló un software [^pysso] que realiza OCR sobre las fotografías y obtiene el peso de cada balanza. Este procedimiento permitió conocer la cantidad exacta de agua de la muestra y en simultáneo la lectura del sensor cada 15 minutos durante un mes, en la {numref}`gravimetry` se observan los componentes del sistema.  


[^pysso]: [Python Seven Segment OCR](https://gitlab.com/emilopez/pysso)



+++

```{figure} /_static/img/auto-gravimetry.png
---
name: gravimetry
alt: fishy
width: 600px
align: center
---
Ensayo gravimétrico automatizado
```

En el siguiente video se observa el proceso de secado de las muestras, confeccionado a partir de la secuencia de fotografías.

```{code-cell} ipython3
:tags: [remove-input]

from IPython.display import HTML

# Youtube
HTML('<center><iframe width="560" height="315" src="https://www.youtube.com/embed/Dlm4kR4mXh4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>')
```

En la {numref}`gravibalanzas` y {numref}`gravisensores` se observa el porcentaje de humedad obtenido a partir de la lectura de las balanzas y las salidas crudas de los sensores de bajo costo. 

+++

```{figure} /_static/img/gravimetry_output_balanza.png
---
name: gravibalanzas
alt: lectura de balanzas
width: 1000px
align: center
---
Lecturas de las balanzas obtenidas mediante OCR
```

```{figure} /_static/img/gravimetry_output_capacitive_sensors.png
---
name: gravisensores
alt: lecturas cruda de sensores de bajo costo
width: 1000px
align: center
---
Lecturas crudas de los sensores de bajo costo para el suelo de tipo 3
```

+++

#### Medición en campo

Se realizaron tres ensayos en campo para los suelos de tipo 1, 2 y 4 ({numref}`Tabla {number}<suelos>`) a una profundidad de entre 7 y 10 centímetros. En todos los casos se registró en forma simultánea con el sensor Hydraprobe II, cuyas lecturas de contenido volumétrico de agua son consideradas los valores de referencia. 

Las variables registradas fueron humedad y temperatura ambiente, temperatura del suelo, capacitancia (sensor de bajo costo) y contenido volumétrico de agua.   

**HACER TABLA CON PERÍODO DE MEDICIÓN**

+++

### Algoritmos de calibración

**por completar**

#### Regresión lineal múltiple

```{math} \hat{y} = \beta_0 + \sum_{i=1}^N \beta_i x_i 
---
label: eqn:mlr
---
```

#### K-nearest neighbor

```{math} \hat{y} = \frac{1}{k} \sum_{x_i \in N(x)} y(x_i) 
---
label: eqn:knn
---
```


#### Random forest

```{math} \hat{y} = \frac{1}{T} \sum_i^T t_i(x) 
---
label: eqn:rfo
---
```


#### Support vector regression

```{code-cell} ipython3
:tags: []

import pandas as pd
import plotly.graph_objects as go
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

data = pd.read_csv('/home/emiliano/data/dev/py-sci-book/UPC-report/mini_book/_static/data/gravimetria.csv', parse_dates=['datetime_b'])
data.head()

idx = (data["humedad_E1"]<=100) | (data["humedad_E2"]<=100) 
data = data[idx]

idx = (data['datetime_b']>'2020-02-18 00:00:00')
data = data[idx]

#c0..c4 = E1
#c6..c10 = E2
X, y = data.iloc[:,12:13], data['humedad_E2']

poly_reg = PolynomialFeatures(degree=3)
X_poly = poly_reg.fit_transform(X)
poly_reg.fit(X_poly,y)

lin_reg2=LinearRegression()
lin_reg2.fit(X_poly,y)
y_pred_poli = lin_reg2.predict(X_poly)


lm = LinearRegression(normalize = True)
lm.fit(X, y)
y_pred_mlr = lm.predict(X)

fig = go.Figure()

fig.add_trace(go.Scattergl(x=data['datetime_b'], y=y_pred_poli, mode='lines', name='ajuste <br>polinómico<br> (n=3)'))
fig.add_trace(go.Scattergl(x=data['datetime_b'], y=y, mode='markers', name='sensor c0'))
fig.update_layout(title='Ajuste polinómico', legend=dict(
    yanchor="top",
    y=0.99,
    xanchor="left",
    x=0.8
))
fig.show()
```

```{code-cell} ipython3
:tags: [remove-input]


```

## Resultados

**por completar**

## Conclusiones
**por completar**

+++ {"tags": ["remove-cell"]}

```{math} e = mc^2
---
label: eqn:best
---
```

This is the best equation {eq}`eqn:best`

```{math} e^{i\pi} + 1 = 0
---
label: euler
---
```

Euler's identity, equation {math:numref}`euler`, was elected one of the
most beautiful mathematical formulas.

```{code-cell} ipython3
:tags: [remove-cell]

import plotly.express as px

df = px.data.iris()
fig = px.scatter(df, x="sepal_width", y="sepal_length", color="species", size="sepal_length")
fig
```

## Trabajo a futuro

### Pendiente

+++

El trabajo realizado será enviado al journal [Agricultural Water Management](https://www.journals.elsevier.com/agricultural-water-management/) para esto primeramente se deberán realizar mediciones durante un tiempo mas prolongado en los suelos de tipo 2 y 4. Esto se realizará durante los meses de abril, mayo y junio de 2021.

Cabe destacar que existe el riesgo de que los sensores de bajo costo no tengan un buen desempeño en suelos de textura fina, esto es limosos/arcillosos (Tipo 2). Frente a este posible esenario habría que replantear la estructura/objetivos de la publicación.  

+++

### Propuesta de colaboración

La idea es montar una grilla de sensores de humedad del suelo en una parcela sin cobertura vegetal. Las mediciones in-situ podrán ser contrastadas y eventualmente calibradas con información sensada remotamente provista por la misión satelital SAOCOM ([ver catálogo](https://catalogos.conae.gov.ar/catalogo/catalogoSatSaocomDocs.html)). 

SAOCOM es una constelación de dos satélites SAR de banda L de alta resolución y cuádruple polarización que observa la Tierra noche y día independientemente de las condiciones climáticas. Forma parte del Sistema Italo Argentino de Satélites para la Gestión de Emergencias ([SIASGE](https://www.argentina.gob.ar/ciencia/conae/misiones-espaciales/siasge)), integrado por los dos satélites SAOCOM provistos por la Comisión Nacional de Actividades Espaciales ([CONAE](https://www.argentina.gob.ar/ciencia/conae)) y cuatro satélites de la Constelación Italiana COSMO-SkyMed, de la Agencia Espacial Italiana (ASI). Los satélites registran la humedad de los 10 primeros centímetros del suelo cada 7 días aproximadamente con una resolución de 1 $m^2$ ({numref}`saocomgrilla`)). 

```{figure} /_static/img/saocom_grilla.png
---
name: saocomgrilla
alt: SAOCOM
width: 600px
align: center
---
Órbita e imágen satelital 
```

El Centro de Estudios Hidroambientales puede adquirir sensores de humedad de suelo de bajo costo y desarrollar la cantidad de dataloggers necesarios para su despliegue en campo. Además, cuenta con 2 sensores Hydraprobe II que se podrían sumar como referencia. El grupo ya ha trabajado y desarrollado software para el procesamiento de imágenes satelitales ([^titi]) y cuenta con investigadores que trabajan en esa línea.  

Para alcanzar este objetivo serían necesarios mas sensores de referencia, una alternativa a los Hydraprobe II son los [EC-5](https://metos.at/portfolio/decagon-ec-5-soil-moisture-sensor/) o [ECH2O 10HS](https://www.metergroup.com/environment/products/ech20-10hs-soil-moisture-sensor/), ambos del fabricante Decagon, cuyo costo varía entre los 150 y 170 euros ([ES op0](https://www.alphaomega-electronics.com/es/sensores-y-sondas/862-10hs-sensor-echo-decagon-para-humedad-de-suelo-gran-volumen.html), [AR op1](https://articulo.mercadolibre.com.ar/MLA-867126754-sensor-de-humedad-de-suelo-decagon-modelo-ec-5-_JM#position=1&type=item&tracking_id=8a14a2a5-26c4-454c-80f0-51efa441aa2f), [ES op2](https://www.alphaomega-electronics.com/es/sensores-y-sondas/1191-ec-5-sensor-de-humedad-de-suelo-decagon.html), [ES op3](https://es.aliexpress.com/i/33054596395.html)).


Existen 3 parcelas disponibles para realizar la medición. La ubicación del sitio de campo se encuentra en el siguiente enlace: [parcela sin cobertura vegetal](http://u.osmfr.org/m/586380/). Una de 15 hectáreas (300mx500m) generalmente utilizada para sembrar maiz que tiene un período de descanso de un mes. Otros campos lindantes que estarían disponibles para la medición tienen suelo sin cobertura vegetal durante mas de un año.

```{figure} /_static/img/campo_sin_cobertura_vegetal.png
---
name: parcelasinvegetacion
alt: parcela sin cobertura vegetal
width: 1000px
align: center
---
Parcela sin cobertura vegetal para medir humedad de suelo
```

```{code-cell} ipython3
:tags: [remove-cell]

from IPython.display import HTML

# Youtube
HTML('<iframe width="100%" height="300px" frameborder="0" allowfullscreen src="http://umap.openstreetmap.fr/es/map/parcela-surface-soil-moisture_586380?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe>')
```

Uno de los objetivos de esta propuesta es obtener un conjunto de datos de humedad de suelo confiables para posteriormente desarrollar algoritmos de machine learning que integren información satelital con datos observados con aceptable precisión y bajo costo, en suelos de la pampa argentina. Por otro lado, se podrían aplicar las técnicas de grafos para reconstruir la información faltante de sensores en alguno de los nodos de la grilla.  

[^titi]: [Titi: Satellite image viewer](https://github.com/emilopez/titi)

+++

```{bibliography}
:style: plain
```
