# Reporte de actividades SANS/UPC

[Emiliano P. López](https://gitlab.com/emilopez) | Centro de Estudios Hidroambientales<BR/> 
[Facultad de Ing. y Cs. Hídricas](http://fich.unl.edu.ar/). <BR/>
[Universidad Nacional del Litoral](https://www.unl.edu.ar/).<BR/> 
Santa Fe - Argentina.

En este reporte se describen las actividades realizadas durante la estancia
que transcurrió del 13/01/2021 al 08/04/2021 en el grupo de investigación [Statistical 
Analysis of Networks and Systems](http://sans.ac.upc.edu/) (SANS) del Departamento de Arquitectura de Computadores 
de la Universidad Politécnica de Cataluña. 

Resumen de las actividades realizadas:

[Calibración de sensores](calibracion_nb)
: Descripción de los sensores utilizados, los ensayos realizados tanto en laboratorio como en campo en distintos tipos de suelo. Calibración usando técnicas de regresión lineal múltiple, k-nearest neighbor, random forest y support vector regression. Análisis de resultados, trabajo pendiente y propuesta de colaboración. 

[CAPTOR-4 bajo consumo](captor_low_power)
: Análisis del consumo de los componentes del sistema. Reducción del consumo por software y hardware. Rediseño del firmware Arduino y del software de comunicación de la Raspberry Pi. 

Cursado de Topics on Optimization and Machine Learning
: Cursado en modalidad oyente, dictado por José María Barceló y Jorge García Vidal.

Grupo de trabajo: Jorge García Vidal, José María Barceló, Pau Ferrer Cid, Aina Main Nadal, Zhe Ye.

```{admonition} Nota
Este documento es navegable e interactivo accediendo al [sitio web](http://ml-regression.gitlab.io/reporte)
```
