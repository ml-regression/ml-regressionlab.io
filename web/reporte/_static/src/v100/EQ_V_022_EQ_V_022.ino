//==============================================================================
// Archivo: EQ_V_02     Abril_2019
// ver.ant. EQ_ana_test_01 y Captor_2_V1
// Arduino NANO 
// PCB  Sensor EQ V1.1  Marzo-2019
// A/D con ref interna  = 1.1V
// AREF Z
//
// Prototipo sensores EQ V1.1
//
// V1:  Setup basico, muestra lecturas analogicas
//      Delay entre lecturas, lectura dummy, promediado 
//      Incluye Comunicacion I2C ( CAptor_2_V1 )
//      concurrencia de varios Nanos (direccion programable) puentes JP1..JP4
//      Comunicacion entre Raspberry Pi (master) y Arduino Nano (slave)
//      incluye gestion de begin/end de canal
//      wire.onRequest(hand)    para responder con datos Funcion principal
//      wire.onReceive(handler) para recepcion de registros
//        Recepcion de registros implementada pero no hace nada
//
// V2:  incorpora ssensor de temperatura y humedad
//
//  String limitado a 32 bytes de datos (usa 28)
//  DD:  2 (1,2)        Dispositivo)
//  A0:  3 (3,4,5)      Analog 0
//  A1:  3 (6,7,8)      Analog 1
//  A2:  3 (9,10,11)    Analog 2
//  A3:  3 (12,13,14)   Analog 3
//  A4:  3 (15,16,17)   Analog 6
//  A5:  3 (18,19,20)   Analog 7
//  TT:  4 (21,22)      Temperatura ºC
//  HR:  2 (23,24)      Humedad Relativa
//  VV:  2 (25,26)      Vacante (alarmas)
//  CK:  2 (27,28)      CRC  (suma en mod 256 de los primeros 26 char (0..25), expresada en 2 digitos HEX)
//
//  el mapa de I2C ocupados se puede obtenet en terminal con "i2cdetect -y 1"
//
//==============================================================================

#include <Wire.h>
#include <DHT11.h>

#define i2c_hexada  0x30   // hexada base para la direccion I2C (sumar nible bajo (JP1..JP4))
#define dispositivo "E1"   // Dispositivo Electroquimico 1 (Solo 2 primeros caracteres)

// JP1..JP4  activos por 0: Abierto:1  cerrado:0
// JP1  peso  1
// JP2  peso  2
// JP3  peso  4
// JP4  peso  8
// Ej:   JP1,JP2 Abierto  JP3, JP4 Cerrados -->   0x33  

//==============================================================================
//         Asignaciones Hardware
//==============================================================================

// Se leen las entradas analogicas:

// A0          pin 19   NO2_WE
// A1          pin 20   NO2_AE
// A2          pin 21   O3+NO2_WE
// A3          pin 22   O3+NO2_AE
// A4   SDA    pin 23
// A5   SCL    pin 24
// A6          pin 25   NO2_AE-WE 
// A7          pin 26   O3+NO2_AE-WE

// selector de dirección del módulo

//  D9         pin 12   JP1  1   Logiga negativa activo por 0
//  D10        pin 13   JP2  2
//  D11        pin 14   JP3  4
//  D12        pin 15   JP4  8

//  D13        pin 17      Onboard LED

#define i2c_add_1    9     
#define i2c_add_2   10
#define i2c_add_4   11
#define i2c_add_8   12

#define LED         13

#define kp  4    // longitud del promedio (pila)

//==============================================================================
//    On Board Led
//==============================================================================

#define DHT_pin 10
DHT11 dht11(DHT_pin);

//==============================================================================
//         Variables Globales
//==============================================================================

//------------------------------------------------------------------------------
//             Para Timers
//------------------------------------------------------------------------------

unsigned long time_a, time_b, time_c, timer_d;   // gestion de timers
long seg_cnt=0;                         // contador de segundos desde inicio

//------------------------------------------------------------------------------
//             Para lectura de datos y gestion local
//------------------------------------------------------------------------------

String buff_0;                          // buffer para string local
int stat_m1=0;                          // estado maquina de estados de captura y filtro de medidas
int AD0[kp+1],AD1[kp+1],AD2[kp+1],AD3[kp+1],AD6[kp+1],AD7[kp+1]; // buffers de lecturas
int ADP_0, ADP_1, ADP_2, ADP_3, ADP_6, ADP_7;                    // valores promediados

//------------------------------------------------------------------------------
//             Para led
//------------------------------------------------------------------------------

unsigned int blinking;                  // patron para led
unsigned int led_ptr;                   // pointer actual a patron de led

//------------------------------------------------------------------------------
//             Para I2C
//------------------------------------------------------------------------------

int i2c_add;                            // direccion i2C completa

// envío de string ( respuesta a peticion de Raspi )
//-----------------------------------------------------

char buff_1[31] = "----------------------------";  // 28 + 3  (10,13,0)

// inicializado
char buf_DI[2] = "E1";     // ( 0,  1)
char buf_A0[3] = "000";    // ( 2,  3,  4)
char buf_A1[3] = "111";    // ( 5,  6,  7)
char buf_A2[3] = "222";    // ( 8,  9, 10)
char buf_A3[3] = "333";    // (11, 12, 13)
char buf_A6[3] = "444";    // (14, 15, 16)
char buf_A7[3] = "555";    // (17, 18, 19)
char buf_TT[2] = "25";     // (20, 21)
char buf_HR[2] = "17";     // (22, 23)
char buf_V1[2] = "VV";     // (24, 25)
char buf_CK[2] = "ck";     // (26, 27)

//  28: 0x10,  29: 0x13,  30: 0x00

int f_peticion;

// recepcion de registros ( enviados por Raspi )
//-----------------------------------------------------

int f_set_registro;
byte r_reg;
byte r_data;

// datos de entrada y de salida serie
//------------------------------------

byte d_entrada=0;
byte d_salida=0;

//Variables de Captor_2_V1
//------------------------

int A0_i=0;
int A1_i=0;
int A2_i=0;
int A3_i=0;
int A4_i=0;
int A5_i=0;
int F_i=0;
int D_i=0;

//------------------------------------------------------------------------------
//             Para Temperatura y Humedad Relativa
//------------------------------------------------------------------------------

unsigned int TT;      // Temperatura
unsigned int HR;      // Humedad Relativa
unsigned int V1;      // Variable 1 (alarmas...)
unsigned int CK;      // CRC

float temp, hum;   // lectura de temperatura y humedad

int err;           // error en lectura del sensor

//==============================================================================
//         SETUP
//==============================================================================

void setup() 
{
buff_0 = "";

analogReference(INTERNAL);     // Fija Referencia analogica a 1.1V
Serial.begin(9600);

time_a = millis();  // inicia referencias para timer
time_b = millis();
time_c = millis();

set_led(10); 

pinMode (9,  INPUT_PULLUP);  // 1
pinMode (10, INPUT_PULLUP);  // 2
pinMode (11, INPUT_PULLUP);  // 4
pinMode (12, INPUT_PULLUP);  // 8
calc_i2c_add();

// Dispositivo y direccion I2C
//Serial.print("Dispositivo  : "); Serial.print(dispositivo);  Serial.println();        // indica Dispositivo
//Serial.print("Direccion I2C: 0x"); Serial.print(i2c_add, HEX); Serial.println();  // inica direccion I2C

Wire.begin(i2c_add);           // Abrimos el canal (i2c_add) del I2C  segun  i2c_hexada + JP1..JP4
Wire.onRequest(peticion);      // Creamos el evento que se realizará cuando el Receptor(Host) llame a el emisor (Este Arduino)
Wire.onReceive(set_registro);  // Creamos el evento que se realizara cuando el Host envia datos para un registro

led_ptr=0;
pinMode(LED, OUTPUT);

TT=25;
HR=17;
V1=0;
CK=0;

}
//==============================================================================
//         LOOP
//==============================================================================

void loop() 
{

act_led();  // actualiza indicación de led


if (millis() >= time_a + 10)    { time_a = millis(); timer_a(); }  // timer para lecturas
if (millis() >= time_b + 100)   { time_b = millis(); timer_b(); }  // timer para led
if (millis() >= time_c + 1000)  { time_c = millis(); timer_c(); }  // timer para mensajes

}
//==============================================================================
//         Timer
//==============================================================================

void timer_a()   // 10 ms
{
ges_m1();  // cada 10 ms ejecuta maquina de estados para lectura de entradas y filtro
}
//==============================================================================

void timer_b()   // 100 ms
{
led_stat();
}
//==============================================================================

void timer_c()   // 1 seg
{
seg_cnt++; 
buff_0 = "";

edita_string();   // num lectura, A0, A1, A2, A3,A6,A7 (en cuentas) Monitor
edita_buf_i2c();  // String normalizado para I2C

//show_temp_hum();


}
//==============================================================================
//
//                        Funciones
//
//==============================================================================

//==============================================================================
//
//              Funciones para servicio del canal I2C
//
//==============================================================================

//===========================================================
//    Peticion de datos desde el Host - Peticion string
//===========================================================
void peticion()
{
f_peticion=1;       // anota peticion para gestion posterior en loop
Wire.write(buff_1);   // envío del buffer disponible para i2c
incre_cnt();        // incrementa contadores ficticios
}

void peticion_ges() // gestion de la peticion desde main
{
Serial.println("Atiende peticion, Enviando buff en i2c...:");   //Imprimimos cuando el receptor pide el mensaje
}

//===========================================================
//         Siguiente contador (demostracion)
//===========================================================
void incre_cnt()
{
  
A0_i++; if (A0_i > 1023)  { A0_i=0; }  // 0..1023
A1_i++; if (A1_i > 1023)  { A1_i=0; }  // 0..1023
A2_i++; if (A2_i > 1023)  { A2_i=0; }  // 0..1023
A3_i++; if (A3_i > 1023)  { A3_i=0; }  // 0..1023
A4_i++; if (A4_i > 1023)  { A4_i=0; }  // 0..1023
A5_i++; if (A5_i > 1023)  { A5_i=0; }  // 0..1023
//F_i++;  if ( F_i > 9999)  {  F_i=0; }  // 0..9999
D_i++;  if ( D_i > 255)   {  D_i=0; }  // 0..255  (8 bits)

A2_i = analogRead(A2);  // ach3
  
}
//===========================================================
//    Peticion de datos desde el Host - Escritura de registro
//===========================================================
// Recepcion de registros implementada pero no hace nada

void set_registro() 
{
f_set_registro=1;
}

void set_registro_ges() 
{
int r_valid=0;
int pos=0;
r_reg=0;
r_data=0;
  
//Serial.print("Escrit_reg...:");   //Imprimimos cuando el host envia datos para un registro

if (Wire.available()) 
  { 
  //Serial.print("lee parametros dispo ");

  while (Wire.available())
    {
    byte a = Wire.read();
    
    //Serial.println(pos);
    
    if (pos==0) { r_reg=a; }
    if (pos==1) { r_data=a; r_valid=1; }
  
    //delay(1); // 1 ms.
    pos++;
    }  
  }

if (r_valid==1)
  {
  Serial.print("Recepcion Valida");
  Serial.print("\t");
  Serial.print("r_reg: ");
  Serial.print(r_reg, HEX);
  Serial.print("\t");
  Serial.print("r_data: ");
  Serial.print(r_data, HEX);
  Serial.println();  

  //if (r_reg==1)  { R1 = r_data; }
  //if (r_reg==2)  { R2 = r_data; }  
  //if (r_reg==3)  { R3 = r_data; }
   
  // si r_reg y r_data correctos... store
  
  }
else
  {
  //Serial.println("recepcion no valida, se descarta");  
  }
}
//==============================================================================
//
//   Calculos del String local   (buff_0)  
//
//==============================================================================

//==============================================================================
// Edita el string buffer_0   para monitor

void edita_string()
{
buff_0="";

add_val(seg_cnt,6); buff_0 = buff_0 + ", ";

buff_0 = buff_0 + "A0:"; add_val(ADP_0,4); buff_0 = buff_0 + ", ";
buff_0 = buff_0 + "A1:"; add_val(ADP_1,4); buff_0 = buff_0 + ", ";
buff_0 = buff_0 + "A2:"; add_val(ADP_2,4); buff_0 = buff_0 + ", ";
buff_0 = buff_0 + "A3:"; add_val(ADP_3,4); buff_0 = buff_0 + ", ";
buff_0 = buff_0 + "A6:"; add_val(ADP_6,4); buff_0 = buff_0 + ", ";
buff_0 = buff_0 + "A7:"; add_val(ADP_7,4);

Serial.println(buff_0);

}
//==============================================================================
// añade un valor al buffer con longitud fija

void add_val(long val, int len)
{
if ((len >= 6) && (val < 100000))  { buff_0 += "0"; }
if ((len >= 5) && (val < 10000))   { buff_0 += "0"; }
if ((len >= 4) && (val < 1000))    { buff_0 += "0"; }
if ((len >= 3) && (val < 100))     { buff_0 += "0"; }
if ((len >= 2) && (val < 10))      { buff_0 += "0"; }  

buff_0 = buff_0 + val;
  
}
//==============================================================================
//
//   Calculos del String para I2C   (buff_1)  
//
//==============================================================================

//==============================================================================
// Edita el string para buffer_1  I2C

void edita_buf_i2c()
{
int k;

// calculo de las variables del buffer  buf_1[31]

ld_dispo();             // DI ( 0,  1)       carga dispositivo
ld_ana();               // A0 ( 2,3,4) A1(5,6,7) A2(8,9,10) A3(11,12,13) A6(14,15,16) A7(17,18,19) Carga valores analogicos
ld_temp();              // TT (20, 21)
ld_HR();                // HR (22, 23)
ld_VV();                // VV (24, 25)

// carga el buffer
k=0;
for (int i=0; i<2; i++)  { buff_1[k] = buf_DI[i]; k++; }  // Dispo
for (int i=0; i<3; i++)  { buff_1[k] = buf_A0[i]; k++; }  // A0
for (int i=0; i<3; i++)  { buff_1[k] = buf_A1[i]; k++; }  // A1
for (int i=0; i<3; i++)  { buff_1[k] = buf_A2[i]; k++; }  // A2
for (int i=0; i<3; i++)  { buff_1[k] = buf_A3[i]; k++; }  // A3
for (int i=0; i<3; i++)  { buff_1[k] = buf_A6[i]; k++; }  // A6
for (int i=0; i<3; i++)  { buff_1[k] = buf_A7[i]; k++; }  // A7
for (int i=0; i<2; i++)  { buff_1[k] = buf_TT[i]; k++; }  // Temp
for (int i=0; i<2; i++)  { buff_1[k] = buf_HR[i]; k++; }  // HR
for (int i=0; i<2; i++)  { buff_1[k] = buf_V1[i]; k++; }  // V1

calc_CRC();             // calcula el CRC (suma mod 256 de los primeros 26 chars expresada en HEX)
ld_CK();                // CK (26, 27)

for (int i=0; i<2; i++)  { buff_1[k] = buf_CK[i]; k++; }  // CRC

buff_1[k] = 0x10; k++;  // terminador de String
buff_1[k] = 0x13; k++;
buff_1[k] = 0x00;

//Serial.print (" buff_1 : "); Serial.print(buff_1); Serial.println();
  
}
//==============================================================================
void ld_dispo()
{
String dispo;
dispo = dispositivo;

buf_DI[0] = dispo[0];
buf_DI[1] = dispo[1];

//Serial.print("dis_tot:"); Serial.println(buf_DI);  
}
//==============================================================================
void ld_ana()
{
String var;

var="";
var=String(ADP_0, HEX);
while (var.length()<3) { var='0'+var; }
for (int i=0; i<3; i++) { buf_A0[i]= var.charAt(i); }

var="";
var=String(ADP_1, HEX);
while (var.length()<3) { var='0'+var; }
for (int i=0; i<3; i++) { buf_A1[i]= var.charAt(i); }

var="";
var=String(ADP_2, HEX);
while (var.length()<3) { var='0'+var; }
for (int i=0; i<3; i++) { buf_A2[i]= var.charAt(i); }

var="";
var=String(ADP_3, HEX);
while (var.length()<3) { var='0'+var; }
for (int i=0; i<3; i++) { buf_A3[i]= var.charAt(i); }

var="";
var=String(ADP_6, HEX);
while (var.length()<3) { var='0'+var; }
for (int i=0; i<3; i++) { buf_A6[i]= var.charAt(i); }

var="";
var=String(ADP_7, HEX);
while (var.length()<3) { var='0'+var; }
for (int i=0; i<3; i++) { buf_A7[i]= var.charAt(i); }
  
}
//==============================================================================
void ld_temp()
{
String var;

var="";
var=String(TT, DEC);
while (var.length()<2) { var='0'+var; }
for (int i=0; i<2; i++) { buf_TT[i]= var.charAt(i); }

}
//==============================================================================
void ld_HR()
{
String var;

var="";
var=String(HR, DEC);
while (var.length()<2) { var='0'+var; }
for (int i=0; i<2; i++) { buf_HR[i]= var.charAt(i); }

}
//==============================================================================
void ld_VV()
{
String var;

var="";
var=String(V1, DEC);
while (var.length()<2) { var='0'+var; }
for (int i=0; i<2; i++) { buf_V1[i]= var.charAt(i); }

}
//==============================================================================
void ld_CK()
{
String var;

var="";
var=String(CK, HEX);
while (var.length()<2) { var='0'+var; }
for (int i=0; i<2; i++) { buf_CK[i]= var.charAt(i); }
}

//==============================================================================

void calc_CRC()
{
// suma los primeros 26 caracteres despreciando parte superior a 8 bits

unsigned char j;

j=0;
for (int i=0; i<26; i++) 
   {
   j=j+buff_1[i];
   //Serial.print(i); Serial.print(" "); Serial.println(buff_1[i],DEC);
   }
CK=j;
//Serial.print("suma:"); Serial.print(" "); Serial.println(CK);
}

//==============================================================================
//
//   Lectura de valores de los sensores y promediado 
//
//==============================================================================

//==============================================================================
//      Maquina de estados para lectura de sensores
//==============================================================================
// en un ciclo de 14 estados, 
// desplaza el grupo de lecturasanteriores
// realiza lectura falsa
// realiza lectura real
// calcula el promedio de cada canal

void ges_m1()
{ 
int v;

switch (stat_m1)   // lee sobre indice 0
{
case  0: {desplaza(); }
case  1: { v      = analogRead(A0); break; }  // Dummy read
case  2: { AD0[0] = analogRead(A0); break; }  // Valid read --> 0
case  3: { v      = analogRead(A1); break; }
case  4: { AD1[0] = analogRead(A1); break; }
case  5: { v      = analogRead(A6); break; }
case  6: { AD6[0] = analogRead(A6); break; }
case  7: { v      = analogRead(A2); break; }
case  8: { AD2[0] = analogRead(A2); break; }
case  9: { v      = analogRead(A3); break; }
case 10: { AD3[0] = analogRead(A3); break; }
case 11: { v      = analogRead(A7); break; }
case 12: { AD7[0] = analogRead(A7); break; }
case 13: { promedio(); }  // promedio
}

stat_m1++;
if (stat_m1>13) { stat_m1=0; }  

}
//==============================================================================

void desplaza()
{
for (int i=kp-1; i>=0; i--)  // desplaza pila
  {
  //Serial.print ("de "); Serial.print (i); Serial.print (" a "); Serial.println (i+1);
    
  AD0[i+1] = AD0[i];
  AD1[i+1] = AD1[i];
  AD2[i+1] = AD2[i];
  AD3[i+1] = AD3[i];
  AD6[i+1] = AD6[i];
  AD7[i+1] = AD7[i];
  }
}
//==============================================================================

void promedio()
{  
//Serial.println("promedio");

ADP_0=0; ADP_1=0; ADP_2=0; ADP_3=0; ADP_6=0; ADP_7=0;  // clear suma

for (int i=0; i<kp; i++)
  {
  ADP_0 = ADP_0 + AD0[i];
  ADP_1 = ADP_1 + AD1[i];  
  ADP_2 = ADP_2 + AD2[i];
  ADP_3 = ADP_3 + AD3[i];  
  ADP_6 = ADP_6 + AD6[i];
  ADP_7 = ADP_7 + AD7[i];
  }

ADP_0 = ADP_0 / kp+1;
ADP_1 = ADP_1 / kp+1;
ADP_2 = ADP_2 / kp+1;
ADP_3 = ADP_3 / kp+1;
ADP_6 = ADP_6 / kp+1;
ADP_7 = ADP_7 / kp+1;

//muestra_hist();
//muestra_prom();

}
//==============================================================================
// muestra el historico de lecturas

void muestra_hist()
{
int i;

Serial.println();
for (i=0; i<=kp; i++) { Serial.print("AD0["); Serial.print(i); Serial.print("]:"); Serial.print(AD0[i]); Serial.print(" "); }; Serial.println();
for (i=0; i<=kp; i++) { Serial.print("AD1["); Serial.print(i); Serial.print("]:"); Serial.print(AD1[i]); Serial.print(" "); }; Serial.println();
for (i=0; i<=kp; i++) { Serial.print("AD2["); Serial.print(i); Serial.print("]:"); Serial.print(AD2[i]); Serial.print(" "); }; Serial.println();
for (i=0; i<=kp; i++) { Serial.print("AD3["); Serial.print(i); Serial.print("]:"); Serial.print(AD3[i]); Serial.print(" "); }; Serial.println();
for (i=0; i<=kp; i++) { Serial.print("AD6["); Serial.print(i); Serial.print("]:"); Serial.print(AD6[i]); Serial.print(" "); }; Serial.println();
for (i=0; i<=kp; i++) { Serial.print("AD7["); Serial.print(i); Serial.print("]:"); Serial.print(AD7[i]); Serial.print(" "); }; Serial.println();
}

//==============================================================================
// muestra l promedio de las lecturas

void muestra_prom()
{
Serial.print("ADP_0: "); Serial.print (ADP_0); Serial.print(" ");
Serial.print("ADP_1: "); Serial.print (ADP_1); Serial.print(" ");
Serial.print("ADP_2: "); Serial.print (ADP_2); Serial.print(" ");
Serial.print("ADP_3: "); Serial.print (ADP_3); Serial.print(" ");
Serial.print("ADP_6: "); Serial.print (ADP_6); Serial.print(" ");
Serial.print("ADP_7: "); Serial.print (ADP_7); Serial.print(" ");
Serial.println();
}
//==============================================================================
//  Calculo de la direccion I2C segun los jumpers JP1, JP2, JP3, JP4
//==============================================================================

void calc_i2c_add()
{
int add;

add = 0;                 // clear
add = add + i2c_hexada;  // suma direccion base

if (digitalRead(i2c_add_1)) { add += 1; }
if (digitalRead(i2c_add_2)) { add += 2; }
if (digitalRead(i2c_add_4)) { add += 4; }
if (digitalRead(i2c_add_8)) { add += 8; }

i2c_add  = add;

}
//==============================================================================
//  Actualiza el estado del led (función de timer b)
//==============================================================================

void led_stat()
{
int L;
led_ptr = led_ptr << 1;
if (led_ptr==0) { led_ptr=1; }
L = (blinking & led_ptr);

if (L!=0) { L=1; }
digitalWrite(LED,L);
  
}
//==============================================================================
//    Gestion del led  (funcion de loop)

void act_led()
{
if (seg_cnt >= 120) { set_led( 1); } // pulso corto
if (seg_cnt < 120) { set_led( 7); } // intermit 1
if (seg_cnt <  60) { set_led( 7); } // intermit 2
if (seg_cnt <  30) { set_led( 8); } // intermit 3
if (seg_cnt <  10) { set_led( 9); } // intermit 4
if (seg_cnt <   5) { set_led(10); } // encendido
  
}
//==============================================================================
//  asigna el patron para el led

void set_led(int code)
{
switch(code)
  {
  case  0:  { blinking=0b0000000000000000; break; } // apagado
  case  1:  { blinking=0b0000000000000001; break; } // pulso corto
  case  2:  { blinking=0b0000000000001001; break; } // dos pulsos
  case  3:  { blinking=0b0000000001001001; break; } // tres pulsos
  case  4:  { blinking=0b0000001001001001; break; } // cuatro pulsos 
  case  5:  { blinking=0b1111000010101000; break; } // tre pulsos con On
  case  6:  { blinking=0b1111111100000000; break; } // intermitente 1
  case  7:  { blinking=0b1111000011110000; break; } // intermitente 2
  case  8:  { blinking=0b1100110011001100; break; } // intermitente 3
  case  9:  { blinking=0b1010101010101010; break; } // intermitente 4
  case 10:  { blinking=0b1111111111111111; break; } // encendido
  default:  { blinking=0b0000000000000000; break; }
  }
}
//==============================================================================
//       Lectura de temperatura y humedad
//==============================================================================

void show_temp_hum()
{

if((err=dht11.read(hum, temp))==0)
  {
  //lcd.setCursor(0, 0); 
  //lcd.print("Tm:");
  //lcd.setCursor(3, 0); lcd.print("     ");
  //lcd.setCursor(3, 0); lcd.print(temp);

  //lcd.setCursor(0, 1); 
  //lcd.print("Hm:");
  //lcd.setCursor(3, 1); lcd.print("     ");
  //lcd.setCursor(3, 1); lcd.print(hum);  

  Serial.print("Temperatura: ");
  Serial.print(temp);
  Serial.print(" Humedad: ");
  Serial.print(hum);
  Serial.println();
  }
else
  {
  //lcd.setCursor(0, 0); 
  //lcd.print("Err:");
  //lcd.setCursor(4, 0); lcd.print("     ");
  //lcd.setCursor(4, 0); lcd.print(err);
      
  Serial.println();
  Serial.print("Error Num :");
  Serial.print(err);
  Serial.println();
  }

//delay(100);

}

//==============================================================================
