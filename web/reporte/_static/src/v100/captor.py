# -*- coding: UTF -*-
#!/bin/python3

########################################################
# prg2_02.py   CAPTOR3       Juli Garcia  Mayo 2019 
# programa para Raspberry Pi, acceso perifericos I2C
# en proto 2
#
# Introduce gestion de error 5 (I/O)
# Introduce escritura de registros
# Introduce decodificacion en funcion del tipo de dispositivo
#
# GPIO 18 -->  pin 12  led
# I2C  envia string de 28 caracteres
#
# añade dos digitos de dispositivo al inicio del string
#
# DI:  2 (1,2)
# A0:  3 (3,4,5)        (1,2,3)
# A1:  3 (6,7,8)        (4,5,6)
# A2:  3 (9,10,11)      (7,8,9)
# A3:  3 (12,13,14)     (10,11,12)
# A4:  3 (15,16,17)     (13,14,15)
# A5:  3 (18,19,20)     (16,17,18)
#  F:  4 (21,22,23,24)  (19,20,21,22)
#  D:  2 (25,26)        (23,24)
#  K:  2 (27,28)        (25,26)
#
########################################################

from smbus import SMBus
from datetime import datetime

import time
import RPi.GPIO as GPIO
import string
import logging

GPIO.setmode(GPIO.BCM)    #
GPIO.setwarnings(False)   # evita info de warnings
GPIO.setup(18, GPIO.OUT)  # led es salida
GPIO.output(18, False)    # led inicialmente apagado

I2C_devices = [0x32, 0x34]


logger = logging.getLogger("captor.log")
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO, filename='captor.log')


# -------------------------------------------------------
# solicita y lee la trama del dispositivo dir_I2C
def read_ardu_str(dir_device):

    bus = SMBus(1)
    try:
        block = bus.read_i2c_block_data(dir_device, 0, 28)  # read 28 char ch 8 --> block
        logger.debug("Message received: {x}".format(x=block))
        bus.close()
        return block

    except IOError:
        logger.debug("Error at read new message")
        time.sleep(0.5)
        bus.close()
        return -1


# -------------------------------------------------------
# Verificacion del CRC
def ck_sum_test(device, block):
    ck_calc = 0
    for i in range(26):
        ck_calc = ck_calc+block[i]

    ck_calc = ck_calc % 256
    st = chr(block[26])+chr(block[27])
    if all(c in string.hexdigits for c in st):
        ck_read = int(st, 16)
        # print(ck_calc, ck_read)
        if ck_read == ck_calc:
            logger.debug("ck_sum OK")
            return 1
        else:
            logger.debug("ck_sum ERROR")
            return 0
    else:
        logger.debug("Message Damaged")
        with open('/home/pi/captor/data/data'+str(device)+'.csv', 'a') as datafile:
            datafile.write("Message Damaged;;;;;;;"+datetime.now().strftime("%Y-%m-%dT%H:%M:%S")+"\n")
        return 0


# -------------------------------------------------------
# decodifica el mensaje recibido
def decode(block):

    st = chr(block[0])+chr(block[1])

    if st == "E1":
        return decode_eq(block)
    # if st == "M1": decode_mo()


# -------------------------------------------------------
# decodificacion en caso de sensores electroquimicos
def decode_eq(block):   # formato E1 Electroquimico
    data = ["E1"]
    # print("decode electroquimico")
    st = chr(block[2])+chr(block[3])+chr(block[4])     # A0
    data.append(int(st, 16))
    st = chr(block[5])+chr(block[6])+chr(block[7])     # A1
    data.append(int(st, 16))
    st = chr(block[8])+chr(block[9])+chr(block[10])    # A2
    data.append(int(st, 16))
    st = chr(block[11])+chr(block[12])+chr(block[13])  # A3
    data.append(int(st, 16))
    st = chr(block[14])+chr(block[15])+chr(block[16])  # A4
    data.append(int(st, 16))
    st = chr(block[17])+chr(block[18])+chr(block[19])  # A5
    data.append(int(st, 16))
    st = chr(block[20])+chr(block[21])  # temp
    data.append(int(st, 10))
    st = chr(block[22])+chr(block[23])  # hr
    data.append(int(st, 10))
    st = chr(block[24])+chr(block[25])  # entr digitales pendiente
    data.append(int(st, 16))
    return data


# -------------------------------------------------------
# solicitud, recepcion y decodificacion de mensaje (trama)
def receive_data():
    for device in I2C_devices:
        block = read_ardu_str(device)
        if block != -1:
            ck_msg = ck_sum_test(device, block)
            if ck_msg == 1:
                data = decode(block)
                if data[0] == "E1":  # Modulo Electroquimico
                    with open('/home/pi/captor/data/data'+str(device)+'.csv', 'a') as datafile:
                        datafile.write(str(data[0]) + ";" + str(data[1]) + ";" + str(data[2]) + ";" + str(data[3])
                                       + ";" + str(data[4]) + ";" + str(data[5]) + ";" + str(data[6])
                                       + ";" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + "\n")

                    logger.debug("<EQ> DI:{di} A0:{a0} A1:{a1} A2:{a2} A3:{a3} A4:{a4} A5:{a5}".format(
                        di=data[0], a0=data[1], a1=data[2], a2=data[3],
                        a3=data[4], a4=data[5], a5=data[6]))

                else:
                    logger.debug("Non Electrochemical Sensor")
        else:
            with open('/home/pi/captor/data/data'+str(device)+'.csv', 'a') as datafile:
                datafile.write("Frame Discarded;;;;;;;"+datetime.now().strftime("%Y-%m-%dT%H:%M:%S")+"\n")
            logger.debug("Frame discarded due to reading error")


# -------------------------------------------------------
#                      MAIN
# -------------------------------------------------------
while True:

    receive_data()     # recepcion de trama (polling)
    time.sleep(1)    # ciclo cada 2 seg

# --------------------------------------------------------
