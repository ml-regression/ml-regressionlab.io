/**
 * @file EQ_V_022.ino
 * @author Emiliano Lopez(emiliano.lopez@gmail.com)
 * @brief based on EQ_V_02
 * @version 1.7
 * @date 2021-03-17
 * 
 */

//==============================================================================
// Archivo: EQ_V_02     MARZO 2021
// ver.ant. EQ_ana_test_01 y Captor_2_V1
// Arduino NANO 
// PCB  Sensor EQ V1.1  Marzo-2019
// A/D con ref interna  = 1.1V
// AREF Z
//
// Prototipo sensores EQ V1.1
//
// V1:  Setup basico, muestra lecturas analogicas
//      Delay entre lecturas, lectura dummy, promediado 
//      Incluye Comunicacion I2C ( CAptor_2_V1 )
//      concurrencia de varios Nanos (direccion programable) puentes JP1..JP4
//      Comunicacion entre Raspberry Pi (master) y Arduino Nano (slave)
//      incluye gestion de begin/end de canal
//      wire.onRequest(hand)    para responder con datos Funcion principal
//      wire.onReceive(handler) para recepcion de registros
//        Recepcion de registros implementada pero no hace nada
//
// V2:  incorpora ssensor de temperatura y humedad
//
//  Paquete de 20 bytes, 12 de datos
//  ts:  4 (1,2,3,4)
//  A0:  2 (5,6)      Analog 0
//  A1:  2 (7,8)      Analog 1
//  A2:  2 (9,10)     Analog 2
//  A3:  2 (11,12)    Analog 3
//  n0:  1 (13)       n values to calc the mean of A0   
//  n1:  1 (14)       n values to calc the mean of A1
//  n2:  1 (15)       n values to calc the mean of A2 
//  n3:  1 (16)       n values to calc the mean of A3
//  CK:  1 (17)       CRC  (suma de los primeros 16 char)
//  END: 3 (18, 19, 20)
//  el mapa de I2C ocupados se puede obtenet en terminal con "i2cdetect -y 1"
//
//==============================================================================
#include <Wire.h>
#include <LowPower.h>
#include "RTClib.h"

RTC_DS1307 rtc;

#define i2c_hexada  0x30   // hexada base para la direccion I2C (sumar nible bajo (JP1..JP4))
#define dispositivo "E1"   // Dispositivo Electroquimico 1 (Solo 2 primeros caracteres)

// JP1..JP4  activos por 0: Abierto:1  cerrado:0
// JP1  peso  1
// JP2  peso  2
// JP3  peso  4
// JP4  peso  8
// Ej:   JP1,JP2 Abierto  JP3, JP4 Cerrados -->   0x33  

//==============================================================================
//         Asignaciones Hardware
//==============================================================================

// Se leen las entradas analogicas:
// A0          pin 19   NO2_WE
// A1          pin 20   NO2_AE
// A2          pin 21   O3+NO2_WE
// A3          pin 22   O3+NO2_AE

// selector de dirección del módulo
//  D9         pin 12   JP1  1   Logiga negativa activo por 0
//  D10        pin 13   JP2  2
//  D11        pin 14   JP3  4
//  D12        pin 15   JP4  8

#define i2c_add_1    9     
#define i2c_add_2   10
#define i2c_add_4   11
#define i2c_add_8   12
#define kp          4                     // longitud del promedio (pila)

//==============================================================================
//         Variables Globales
//==============================================================================
char ID[2] = "E1";
unsigned long time_a, time_b, time_c;            // gestion de timers
unsigned long NOBS = 10;                         // Number of continuous observations = [1 .. 60]
unsigned long Tsleep = (900/60)*NOBS - NOBS;     // Sleeping time in seconds: 900*NOBS/60 - NOBS
int  stat_m1 = 0;                                // estado maquina de estados de captura y filtro de medidas
uint16_t  AD0[kp+1], AD1[kp+1], AD2[kp+1], AD3[kp+1]; // buffers de lecturas
int  ADP_0, ADP_1, ADP_2, ADP_3;                 // valores promediados
int  raw_data[4][60], idx_data=0;                // a row for each sensor, columns for measures: one per second for one minute
uint16_t  data_i2c_packet[4];                    // average of the raw data, one for each sensor
byte ndata_i2c_packet[4];                        // number of samples used for previous averages
byte kpacket = 0;                                // number of packet are being built: one every 15 minutes for 2 hours: 8 packets
char packets[8][25];
char n_request=0;
bool first_request = true;                       // primer request envía metadato: id del dispositivo y cant de paquetes actuales
int i2c_add;                                     // direccion i2C completa
bool wakeUp = true;
uint16_t sleep_times = 0;

//==============================================================================
//         SETUP
//==============================================================================

void setup(){
    
    analogReference(INTERNAL);     // Fija Referencia analogica a 1.1V
    Serial.begin(9600);
    
    time_a = millis();  // inicia referencias para timer
    time_b = millis();
    time_c = millis();
    
    pinMode (9,  INPUT_PULLUP);  // 1
    pinMode (10, INPUT_PULLUP);  // 2
    pinMode (11, INPUT_PULLUP);  // 4
    pinMode (12, INPUT_PULLUP);  // 8
    calc_i2c_add();
    
    // Dispositivo y direccion I2C
    Serial.print("Direccion I2C: 0x"); Serial.println(i2c_add, HEX); // inica direccion I2C
    
    Wire.begin(i2c_add);           // join i2c bus with address i2c_add (i2c_hexada + JP1..JP4)
    Wire.onRequest(send_packet);   // Creamos el evento que se realizará cuando el Receptor(Host)(RPI) llame a el emisor (Este Arduino)
    Wire.onReceive(receive_NOBS);  //
}
//==============================================================================
//         LOOP
//==============================================================================
// start                    13:16:20.150
// 1º packet: 2021-03-17 12:16:39
// 2º packet: 2021-03-17 12:28:22
// 3º packet: 2021-03-17 12:40:01
// 4º packet: 2021-03-17 12:51:23'
void loop(){
    if (wakeUp){
        bool test = false;
        if (millis() >= time_a + 10)        {time_a = millis(); read_prepare_data(test); }      // read sensors every 10 ms
        if (millis() >= time_b + 1000)      {time_b = millis(); save_data_build_packet();}      // save data every second
        if (millis() >= time_c + NOBS*1000) {time_c = millis(); wakeUp = false;}                // go to sleep after 60 seconds
    }else {
        delay(50);
        LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);          // sleep for 8 seconds
        sleep_times += 1;
        if (sleep_times >= Tsleep/8){                            // it's time to wakeup!
            wakeUp = true;                                       // flag to wakeup
            sleep_times = 0;                                     // reset sleep times counter
            time_b = millis(); 
            time_c = millis();                                   // update timer to sleep  
            //Serial.println("w");
        }
    }
}



/**
 * @brief save the computed moving average of each sensor as an observation.
 * if is the 60º observation compute the average excluding outliers using z-score 
 * and build a new packet to be added to the i2c buffer 
 */
void save_data_build_packet(){                          // save data on each row every second, and after a minute calc averages
    //Serial.print(".");
    raw_data[0][idx_data] = ADP_0;                      // moving average for analog 0
    raw_data[1][idx_data] = ADP_1;                      // moving average for analog 1
    raw_data[2][idx_data] = ADP_2;                      // moving average for analog 2
    raw_data[3][idx_data] = ADP_3;                      // moving average for analog 3
    idx_data++;
     
    if (idx_data >= 60){                                // BUILD PACKET=60th counter is the last value of a minute: compute avg without outliers 
        idx_data = 0;                                   // reset index for next 60 seconds data values 
        for (uint8_t sensor=0; sensor<4; sensor++){          
            long _mean = mean(raw_data[sensor], 60);    // mean: for each data sensor
            long   _sd = sd(raw_data[sensor], 60);      // standar deviation
            long   avg = 0;
            byte     k = 0;

            if (_sd == 0){                              // standar deviation = 0 => all the values are the same
                avg = raw_data[sensor][0];
                k = 60;
            }else{                                      // when std is not 0, compute zscore to avoid outliers
                for (byte i=0;i<60;i++){                // zscore to detect outliers ((x-mean)/sd): https://en.wikipedia.org/wiki/Standard_score / https://towardsdatascience.com/z-score-for-anomaly-detection-d98b0006f510
                    long zscore = ( abs(raw_data[sensor][i] - _mean) ) / _sd;  
                    if (zscore < 2){                    // good if the zscore is less than 2
                        avg += raw_data[sensor][i];
                        k++;
                    }
                }
                avg = avg / k;
            }
            data_i2c_packet[sensor]  = avg;             // average without outliers -> to be sent
            ndata_i2c_packet[sensor] = k;               // k good values            -> to be sent
        }
        build_packet_add_buffer();
    }
}
/**
 * @brief add each variable to the packet. Using memcpy 
 * for data greater than a byte
 */
void build_packet_add_buffer(){
    // - tengo que armar el paquete kpacket en data_i2c_packet de hasta 32 bytes para mandarlo por i2c
    // - usar memcpy
    // voy a tener: 2 bytes de ID de dispositiva
    //              4 bytes de unix timestamp:                  ts
    //              8 bytes = 4 int de promedio de datos,       data
    //              4 bytes de N datos validos en cada promedio nd
    //              2 bytes para temperatura: int (*10 y trunc)
    //              1 byte para humedad (char: -128 a 127)
    //              --> 21 bytes of data to calc CRC
    //              1 bytes de CRC                              C  // HASTA ACA LEO CON RPi
    //              3 bytes de fin                              end
    //              ===============
    //      total = 25 bytes
    // Packet 
    // +--+----+--------+----+--+-+-+---+
    // |id| ts | AN data| nd |Te|H|C|end|
    // +--+----+--------+----+--+-+-+---+
    float temp = -50.34;                 // hardcoded temp -> to be replaced with DHT22 reading
    float hume = 99.9;                   // idem
    int  t = (int)(temp * 100);          // shift comma and truncate decimals
    char h = (int)hume;                  // trunc decimal
    DateTime now = rtc.now();
    uint32_t unix_time = now.unixtime(); //uint32_t unix_time = 1615622433;

    byte CRC = 0;
    // copies n bytes from memory: memcpy(void *dest, const void * src, size_t n)
    memcpy((packets[kpacket]),      (byte *)(&ID),              2);   // 2 bytes for ID
    memcpy((packets[kpacket] + 2),  (byte *)(&unix_time),       4);   // copy 4 bytes from timestamp to packet
    memcpy((packets[kpacket] + 6),  (byte *)(&data_i2c_packet), 8);   // copy 8 bytes from data to packet
    memcpy((packets[kpacket] + 14), (byte *)(&ndata_i2c_packet),4);   // copy 4 bytes from ndata to packet
    memcpy((packets[kpacket] + 18), (byte *)(&t),               2);   // copy 2 bytes from temp
    packets[kpacket][20] = h;                                         // a byte for HR
    for (byte i=0; i<21; i++) { CRC = CRC + packets[kpacket][i]; }    // calc CRC
    packets[kpacket][21] = CRC;                                       // assign CRC single byte 
    packets[kpacket][22] = 10;                                        // a byte end1
    packets[kpacket][23] = 13;                                        // a byte end2
    packets[kpacket][24] = 0;                                         // a byte end3

    //decode_values();  // to check if memcpy is working fine
    kpacket++;                                      // index for the packet: one packet every 15 minutes for 2 hours
    if (kpacket>7) {kpacket = 0;}                   // reset index packet
}

/**
 * @brief decode saved values in the packet to check its content
 */
void decode_values(){
    // decode:      4 bytes de unix timestamp:                  ts
    //              8 bytes = 4 int de promedio de datos,       data
    //              4 bytes de N datos validos en cada promedio nd
    //              --> 16 bytes of data to calc CRC
    //              1 bytes de CRC                              C
    //              3 bytes de fin                              end
    //              ===============
    //      total = 20 bytes
    
    long _timestamp;
    uint16_t dataA0, dataA1, dataA2, dataA3;
    uint8_t ndA0, ndA1, ndA2, ndA3, _crc, end1, end2, end3;
    for (byte i=0; i <= kpacket; i++){
        memcpy((&_timestamp),(byte *)(packets[i]),4);
        memcpy((&dataA0),(byte *)(packets[i]+4),2);
        memcpy((&dataA1),(byte *)(packets[i]+6),2);
        memcpy((&dataA2),(byte *)(packets[i]+8),2);
        memcpy((&dataA3),(byte *)(packets[i]+10),2);
        memcpy((&ndA0),(byte *)(packets[i]+12),1);
        memcpy((&ndA1),(byte *)(packets[i]+13),1);
        memcpy((&ndA2),(byte *)(packets[i]+14),1);
        memcpy((&ndA3),(byte *)(packets[i]+15),1);
        memcpy((&_crc),(byte *)(packets[i]+16),1);
        memcpy((&end1),(byte *)(packets[i]+17),1);
        memcpy((&end2),(byte *)(packets[i]+18),1);
        memcpy((&end3),(byte *)(packets[i]+19),1);
    
        Serial.print("# ");
        Serial.print(i);
        Serial.print(" :");
        Serial.print(_timestamp);
        Serial.print(" . ");
        Serial.print(dataA0);
    
        Serial.print(" . ");
        Serial.print(dataA1);
    
        Serial.print(" . ");
        Serial.print(dataA2);
    
        Serial.print(" . ");
        Serial.print(dataA3);

        Serial.print(" | ");
        Serial.print(ndA0);
        Serial.print(" . ");
        Serial.print(ndA1);
        Serial.print(" . ");
        Serial.print(ndA2);
        Serial.print(" . ");
        Serial.print(ndA3);
        Serial.print(" | ");
        Serial.print(_crc);
        Serial.print(" | ");
        Serial.print(end1);
        Serial.print(" . ");
        Serial.print(end2);
        Serial.print(" . ");
        Serial.println(end3);
    }    
}
/**
 * @brief compute the mean of vector
 * 
 * @param v vector of measurements
 * @param N length of the vector v
 * @return long integer mean (truncated)
 */
long mean(int v[], int N){
    long m = 0;
    for (int i=0;i<N;i++){
        m += v[i];
    }
    return m/N;
}

/**
 * @brief compute standar deviation
 * 
 * @param v input vector of measurement
 * @param N length of the vector
 * @return int standar deviation
 */
int sd(int v[], int N){
    long p = mean(v, N);
    long _sd = 0;
    for (int i = 0; i < N; i++){
        _sd += pow((v[i]-p), 2);
    }
    _sd = sqrt(_sd/(N-1));
    return _sd;
}
/**
 * @brief Called by the onRequest I2C event (from Host: Raspberry Pi).
 *        Send the 8 packets in the buffer
 * 
 */
void send_packet(){                  
    Wire.write(packets[n_request]);    // envío i2c el paquete n_request
    n_request++;
    if (n_request>7) {n_request = 0; }
}

void receive_NOBS(){
    char nobs_i2c;
    while(Wire.available()) {  // read all bytes received
        nobs_i2c = Wire.read();
    }
    Serial.println((byte)nobs_i2c);
}

/**
 * @brief State machine for sensors reading.1) shift previous readings. 
 * 2) Dummy reading. 3) Read sensor and assign at the first place of the vector.
 * 4) Compute the mean of the values on each vector of readings 
 * 
 * @param test if true generate random values insted of readings 
 * analog ports, if false read the measurements from the sensors
 */
void read_prepare_data(bool test){ 
    int dummy;
    switch (stat_m1){                                 // lee sobre indice 0
        case  0: { desplaza(); break;}
        case  1: { dummy  = analogRead(A0); break; }  // Dummy read
        case  2: { AD0[0] = analogRead(A0); break; }  // Valid read --> 0
        case  3: { dummy  = analogRead(A1); break; }
        case  4: { AD1[0] = analogRead(A1); break; }
        case  5: { dummy  = analogRead(A2); break; }
        case  6: { AD2[0] = analogRead(A2); break; }
        case  7: { dummy  = analogRead(A3); break; }
        case  8: { AD3[0] = analogRead(A3); break; }
        case  9: { promedio(); }  // promedio
    }
    if (test){
          randomSeed(millis());
          AD0[0] = 500 + random(500);
          AD1[0] = 500 + random(500);
          AD2[0] = 500 + random(500);
          AD3[0] = 500 + random(500); 
    }
    
    stat_m1++;
    if (stat_m1>9){ stat_m1=0; }  

}
//-------------------------------------------------
void desplaza(){
    //Serial.println("desplazando");
    for (int i = kp-1; i >= 0; i--){  // desplaza pila        
        AD0[i+1] = AD0[i];
        AD1[i+1] = AD1[i];
        AD2[i+1] = AD2[i];
        AD3[i+1] = AD3[i];
    }
}
//-------------------------------------------------
void promedio(){  
    //Serial.println("promedio");

    ADP_0=0; ADP_1=0; ADP_2=0; ADP_3=0;  // clear suma
    
    for (int i=0; i < kp+1; i++){
        ADP_0 = ADP_0 + AD0[i];
        ADP_1 = ADP_1 + AD1[i];  
        ADP_2 = ADP_2 + AD2[i];
        ADP_3 = ADP_3 + AD3[i];  
    }
    
    ADP_0 = ADP_0 / (kp+1);
    ADP_1 = ADP_1 / (kp+1);
    ADP_2 = ADP_2 / (kp+1);
    ADP_3 = ADP_3 / (kp+1);
}

//  Calculo de la direccion I2C segun los jumpers JP1, JP2, JP3, JP4
//==================================================================
void calc_i2c_add(){
    byte add;
    
    add = 0;                 // clear
    add = add + i2c_hexada;  // suma direccion base
    
    if (digitalRead(i2c_add_1)) { add += 1; }
    if (digitalRead(i2c_add_2)) { add += 2; }
    if (digitalRead(i2c_add_4)) { add += 4; }
    if (digitalRead(i2c_add_8)) { add += 8; }
    
    i2c_add  = add;
}
