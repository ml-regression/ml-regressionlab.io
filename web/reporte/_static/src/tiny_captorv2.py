# -*- coding: UTF -*-
#!/bin/python3

########################################################
# tiny captor
########################################################

from smbus import SMBus
from datetime import datetime
import time
import RPi.GPIO as GPIO
import logging

GPIO.setmode(GPIO.BCM)    #
GPIO.setwarnings(False)   # evita info de warnings
GPIO.setup(18, GPIO.OUT)  # led es salida
GPIO.output(18, False)    # led inicialmente apagado

logger = logging.getLogger("captor.log")
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO, filename='captor.log')

def get_data_from_arduino(i2c_addr):
    """get one data packet

    :param i2c_addr: i2c address 
    :type i2c_addr: hexadecimal
    :return: list of bytes
    :rtype: list()
    """
    try:
        bus = SMBus(1)
        block = bus.read_i2c_block_data(i2c_addr, 0, 22)  # read 22 bytes
        logger.debug("Message received: {x}".format(x=block))
        bus.close()
        return block
    except IOError:
        logger.debug("Error at read new message")
        time.sleep(0.5)
        bus.close()
        return -1
    
def calc_CRC(data):
    """compute the CRC,sum of data,  %256

    :param data: sequence of numbers
    :type data: list of numbers
    :return: CRC value of data
    :rtype: int
    """
    crc = 0
    for d in data:
        crc += d
    return crc%256 

def decode(a_packet):
    """decode a packet sent by arduino

    :param a_packet: list of bytes
    :type a_packet: list()
    :return: list of decoded bytes
    :rtype: list()
    """
    rawID = a_packet[0:2] 
    # timestamp first 4 bytes
    raw_timestamp = a_packet[2:6]
    rawA0 = a_packet[6:8]
    rawA1 = a_packet[8:10]
    rawA2 = a_packet[10:12]
    rawA3 = a_packet[12:14]
    # n data used to average
    NA0 = a_packet[14]
    NA1 = a_packet[15]
    NA2 = a_packet[16]
    NA3 = a_packet[17]
    # temp and HR
    rawTe = a_packet[18:20]
    hr    = a_packet[20]
    # CRC
    CRC = a_packet[21]
    # decode raw values
    unix_timestamp = int.from_bytes(bytearray(raw_timestamp), byteorder='little')
    A0 = int.from_bytes(bytearray(rawA0), byteorder='little')
    A1 = int.from_bytes(bytearray(rawA1), byteorder='little')
    A2 = int.from_bytes(bytearray(rawA2), byteorder='little')
    A3 = int.from_bytes(bytearray(rawA3), byteorder='little')
    te = int.from_bytes(bytearray(rawTe), byteorder='little')
    ID = chr(rawID[0]) + chr(rawID[1])
    if te > 32767:
        te = -1*(65536-te) 
    te = te/100.0

    return [ID, unix_timestamp, A0, A1, A2, A3, NA0, NA1, NA2, NA3, te, hr, CRC]

def valid_CRC(p, posCRC=21):
    """check if CRC in the packet is valid

    :param a_packet: list of numbers sent by Arduino
    :type a_packet: list
    :param posCRC: CRC index in the list, defaults to 21
    :type posCRC: int, optional
    :return: True or False
    :rtype: bool
    """
    check = p[posCRC] == calc_CRC(p[0:posCRC])
    if check:
        logger.debug("ck_sum OK")
    else:
        logger.debug("ck_sum ERROR")
    return check

def main(PATH='/home/pi/captor/data', DEVICE={'id':'E1', 'addr':0x3D}):
    # read 8 packets from arduino by i2c channel
    received_packets = []
    for i in range(8):
        received_packets.append(get_data_from_arduino(DEVICE['addr']))     	# recepcion de trama (polling)
        time.sleep(0.1)
    print('# Raw data')
    for rp in received_packets:
        print(rp)
    # decode valid data
    decoded_data = [decode(p) for p in received_packets if valid_CRC(p)]
    # decoded data
    print('# Decoded data')
    for p in decoded_data:
        print(p)

    # human readable data: unixtime -> yyyy-mm-dd hh:mm:ss
    print('# Human readable data')
    for p in decoded_data:
        unix_timestamp = p[1]
        device_id      = p[0]
        try:
            timestamp = datetime.fromtimestamp(unix_timestamp).strftime('%Y-%m-%d %H:%M:%S')
        except:
            timestamp = ''
        if DEVICE['id'] == device_id:
            data  =  [timestamp] + p
            row   = ';'.join([str(x) for x in data])
            fname = f'{PATH}/data{device_id}.csv'
            try:
                with open(fname, 'a') as datafile:
                    datafile.write(row + '\n')
            except Exception as e:
               print(e)
        print(row)

# --------------------------------------------------------

if __name__ == "__main__":
    DEVICES = [{'id':'E1', 'addr':0x3D}]
    for a_device in DEVICES:
        main(DEVICE=a_device)
