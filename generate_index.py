from string import Template

# template de lectura y archivo de salida
index_output  = open("/home/emiliano/data/dev/ml-regression.gitlab.io/web/index.html", "w")
template_html = open("/home/emiliano/data/dev/ml-regression.gitlab.io/web/index_template.html", "r").read()
template_tmp  = Template(template_html)

# figuras generadas con plotly y purgadas por ahora a mano -> nada mas que sacar el html y body
DIR = "/home/emiliano/data/dev/lml/viz"

msm  = open(f"{DIR}/measurements.html", "r").read()
scr  = open(f"{DIR}/scatter_raw.html", "r").read()
box  = open(f"{DIR}/box_plot_raw.html", "r").read()
s3d5 = open(f"{DIR}/scatter3d_ic5.html", "r").read()
s05  = open(f"{DIR}/sensor_i0_5.html", "r").read()
acc  = open(f"{DIR}/rmse_R2.html", "r").read()
sc05 = open(f"{DIR}/scatter_sensor_i0_5.html", "r").read()

index_content = template_tmp.substitute(measurements     = msm,
                                        scatter_raw      = scr,
                                        boxplot_raw      = box,
                                        scatter3d_raw_i5 = s3d5,
                                        predictions_i0_5 = s05,
                                        accuracy         = acc,
                                        scatter_i0_5     = sc05,
                                        )

index_output.write(index_content)
index_output.close()